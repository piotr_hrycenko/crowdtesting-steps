import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CrowdtestingTestModule } from '../../../test.module';
import { TesterComponent } from 'app/entities/tester/tester.component';
import { TesterService } from 'app/entities/tester/tester.service';
import { Tester } from 'app/shared/model/tester.model';

describe('Component Tests', () => {
  describe('Tester Management Component', () => {
    let comp: TesterComponent;
    let fixture: ComponentFixture<TesterComponent>;
    let service: TesterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CrowdtestingTestModule],
        declarations: [TesterComponent],
        providers: []
      })
        .overrideTemplate(TesterComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TesterComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TesterService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Tester(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.testers && comp.testers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
