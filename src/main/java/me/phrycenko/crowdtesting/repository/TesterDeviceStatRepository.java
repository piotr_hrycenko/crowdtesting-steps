package me.phrycenko.crowdtesting.repository;

import me.phrycenko.crowdtesting.domain.projection.TesterStatProjection;

import java.util.List;


/**
 * Spring Data  repository for the TesterStatProjection.
 */
public interface TesterDeviceStatRepository {

    List<TesterStatProjection> findAllTopExperienced(int pageSize, int offset);

    List<TesterStatProjection> findAllByCountryTopExperienced(List<String> countries, int pageSize, int offset);

    List<TesterStatProjection> findAllByDeviceIdsTopExperienced(List<Integer> deviceIds, int pageSize, int offset);

    List<TesterStatProjection> findAllByCountryAndDeviceIdsTopExperienced(List<String> countries,
                                                                          List<Integer> deviceIds,
                                                                          int pageSize, int offset);

}
