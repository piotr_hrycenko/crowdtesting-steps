package me.phrycenko.crowdtesting.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import me.phrycenko.crowdtesting.web.rest.TestUtil;

public class TesterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tester.class);
        Tester tester1 = new Tester();
        tester1.setId(1L);
        Tester tester2 = new Tester();
        tester2.setId(tester1.getId());
        assertThat(tester1).isEqualTo(tester2);
        tester2.setId(2L);
        assertThat(tester1).isNotEqualTo(tester2);
        tester1.setId(null);
        assertThat(tester1).isNotEqualTo(tester2);
    }
}
