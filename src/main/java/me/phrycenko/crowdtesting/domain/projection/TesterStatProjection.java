package me.phrycenko.crowdtesting.domain.projection;

import java.io.Serializable;

public class TesterStatProjection implements Serializable {

    private Long testerId;

    private String firstName;

    private String lastName;

    private Integer bugsAmount;

    public TesterStatProjection(Long testerId, String firstName, String lastName, Integer bugsAmount) {
        this.testerId = testerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.bugsAmount = bugsAmount;
    }

    public Long getTesterId() {
        return testerId;
    }

    public void setTesterId(Long testerId) {
        this.testerId = testerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getBugsAmount() {
        return bugsAmount;
    }

    public void setBugsAmount(Integer bugsAmount) {
        this.bugsAmount = bugsAmount;
    }
}
