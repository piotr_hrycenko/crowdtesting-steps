package me.phrycenko.crowdtesting.repository;

import me.phrycenko.crowdtesting.domain.Tester;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Tester entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TesterRepository extends JpaRepository<Tester, Long> {

}
