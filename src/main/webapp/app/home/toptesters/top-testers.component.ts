import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { ITesterStatProjection } from 'app/shared/model/projection/tester-stat-projection.model';
import { TopTestersService } from 'app/home/toptesters/top-testers.service';
import { HttpResponse } from '@angular/common/http';
import { Country } from 'app/shared/model/enumerations/country.model';
import { IDevice } from 'app/shared/model/device.model';
import { DeviceService } from 'app/entities/device/device.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

export interface ICountryEntry {
  id: any;
  country: Country;
}

export class CountryEntry implements ICountryEntry {
  constructor(public id: any, public country: Country) {}
}

@Component({
  selector: 'jhi-top-testers',
  templateUrl: './top-testers.component.html',
  styleUrls: ['top-testers.scss']
})
export class TopTestersComponent implements OnInit {
  topTesters: ITesterStatProjection[];

  countries: CountryEntry[] = [];
  selectedCountries = [];

  devices: IDevice[] = [];
  selectedDevices = [];

  itemsPerPage: number;
  page: number;
  noMoreItemsAvailable: boolean;

  needToConfirmCriteria: boolean;
  topTestersLoadInProgress: boolean;

  constructor(private topTestersService: TopTestersService, private deviceService: DeviceService) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.noMoreItemsAvailable = false;
    this.topTesters = [];

    this.needToConfirmCriteria = true;
    this.topTestersLoadInProgress = false;
  }

  ngOnInit(): void {
    this.loadCountries();
    this.loadDevices();
  }

  /**
   * Initial data load
   */

  loadCountries(): void {
    const countryKeys = Object.keys(Country);
    this.countries = countryKeys.slice(countryKeys.length / 2).map<CountryEntry>((k: string) => new CountryEntry(k, CountryEntry[k]));
  }

  loadDevices(): void {
    this.deviceService
      .query()
      .pipe(
        map((res: HttpResponse<IDevice[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IDevice[]) => (this.devices = resBody));
  }

  /**
   * Event listeners
   */

  onFilterChanged(): void {
    this.needToConfirmCriteria = true;
  }

  search(): void {
    this.page = 0;
    this.noMoreItemsAvailable = false;
    this.topTesters = [];
    this.needToConfirmCriteria = false;
    this.loadTopTesters();
  }

  /**
   * View manipulation
   */

  shouldShowSearchClickNeededInfo(): boolean {
    return this.needToConfirmCriteria;
  }

  shouldShowNoResultsInfo(): boolean {
    return this.topTesters.length === 0 && !this.needToConfirmCriteria && !this.topTestersLoadInProgress;
  }

  shouldShowTopTestersList(): boolean {
    return this.topTesters.length > 0 && !this.needToConfirmCriteria;
  }

  shouldShowTopTestersLoadInProgressInfo(): boolean {
    return this.topTestersLoadInProgress;
  }

  /**
   * Others
   */

  loadTopTesters(): void {
    this.topTestersLoadInProgress = true;
    this.topTestersService
      .query({
        offset: this.page * this.itemsPerPage,
        pageSize: this.itemsPerPage,
        countries: this.selectedCountries,
        devices: this.selectedDevices
      })
      .subscribe((res: HttpResponse<ITesterStatProjection[]>) => this.paginateTopTesters(res.body));
  }

  protected paginateTopTesters(data: ITesterStatProjection[] | null): void {
    if (data) {
      this.page = this.page + 1;
      this.noMoreItemsAvailable = data.length < this.itemsPerPage;
      for (let i = 0; i < data.length; i++) {
        this.topTesters.push(data[i]);
      }
    }
    this.topTestersLoadInProgress = false;
  }
}
