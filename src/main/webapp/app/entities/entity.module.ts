import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'device',
        loadChildren: () => import('./device/device.module').then(m => m.CrowdtestingDeviceModule)
      },
      {
        path: 'tester',
        loadChildren: () => import('./tester/tester.module').then(m => m.CrowdtestingTesterModule)
      },
      {
        path: 'bug',
        loadChildren: () => import('./bug/bug.module').then(m => m.CrowdtestingBugModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class CrowdtestingEntityModule {}
