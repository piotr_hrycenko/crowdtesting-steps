export interface IBug {
  id?: number;
  deviceId?: number;
  testerId?: number;
}

export class Bug implements IBug {
  constructor(public id?: number, public deviceId?: number, public testerId?: number) {}
}
