package me.phrycenko.crowdtesting.service.dto;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link me.phrycenko.crowdtesting.domain.Bug} entity.
 */
@ApiModel(description = "The Bug entity.")
public class BugDTO implements Serializable {

    private Long id;


    private Long deviceId;

    private Long testerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getTesterId() {
        return testerId;
    }

    public void setTesterId(Long testerId) {
        this.testerId = testerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BugDTO bugDTO = (BugDTO) o;
        if (bugDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bugDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BugDTO{" +
            "id=" + getId() +
            ", deviceId=" + getDeviceId() +
            ", testerId=" + getTesterId() +
            "}";
    }
}
