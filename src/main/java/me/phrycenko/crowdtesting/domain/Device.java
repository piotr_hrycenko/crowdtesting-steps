package me.phrycenko.crowdtesting.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The Device entity.
 */
@Entity
@Table(name = "device")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "device_tester",
               joinColumns = @JoinColumn(name = "device_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tester_id", referencedColumnName = "id"))
    private Set<Tester> testers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Device description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Tester> getTesters() {
        return testers;
    }

    public Device testers(Set<Tester> testers) {
        this.testers = testers;
        return this;
    }

    public Device addTester(Tester tester) {
        this.testers.add(tester);
        tester.getDevices().add(this);
        return this;
    }

    public Device removeTester(Tester tester) {
        this.testers.remove(tester);
        tester.getDevices().remove(this);
        return this;
    }

    public void setTesters(Set<Tester> testers) {
        this.testers = testers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Device)) {
            return false;
        }
        return id != null && id.equals(((Device) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Device{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
