import { ITester } from 'app/shared/model/tester.model';

export interface IDevice {
  id?: number;
  description?: string;
  testers?: ITester[];
}

export class Device implements IDevice {
  constructor(public id?: number, public description?: string, public testers?: ITester[]) {}
}
