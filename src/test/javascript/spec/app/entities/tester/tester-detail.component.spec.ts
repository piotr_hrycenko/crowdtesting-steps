import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CrowdtestingTestModule } from '../../../test.module';
import { TesterDetailComponent } from 'app/entities/tester/tester-detail.component';
import { Tester } from 'app/shared/model/tester.model';

describe('Component Tests', () => {
  describe('Tester Management Detail Component', () => {
    let comp: TesterDetailComponent;
    let fixture: ComponentFixture<TesterDetailComponent>;
    const route = ({ data: of({ tester: new Tester(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CrowdtestingTestModule],
        declarations: [TesterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TesterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TesterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tester on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tester).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
