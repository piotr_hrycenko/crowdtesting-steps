import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IBug, Bug } from 'app/shared/model/bug.model';
import { BugService } from './bug.service';
import { IDevice } from 'app/shared/model/device.model';
import { DeviceService } from 'app/entities/device/device.service';
import { ITester } from 'app/shared/model/tester.model';
import { TesterService } from 'app/entities/tester/tester.service';

type SelectableEntity = IDevice | ITester;

@Component({
  selector: 'jhi-bug-update',
  templateUrl: './bug-update.component.html'
})
export class BugUpdateComponent implements OnInit {
  isSaving = false;

  devices: IDevice[] = [];

  testers: ITester[] = [];

  editForm = this.fb.group({
    id: [],
    deviceId: [],
    testerId: []
  });

  constructor(
    protected bugService: BugService,
    protected deviceService: DeviceService,
    protected testerService: TesterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bug }) => {
      this.updateForm(bug);

      this.deviceService
        .query()
        .pipe(
          map((res: HttpResponse<IDevice[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IDevice[]) => (this.devices = resBody));

      this.testerService
        .query()
        .pipe(
          map((res: HttpResponse<ITester[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: ITester[]) => (this.testers = resBody));
    });
  }

  updateForm(bug: IBug): void {
    this.editForm.patchValue({
      id: bug.id,
      deviceId: bug.deviceId,
      testerId: bug.testerId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bug = this.createFromForm();
    if (bug.id !== undefined) {
      this.subscribeToSaveResponse(this.bugService.update(bug));
    } else {
      this.subscribeToSaveResponse(this.bugService.create(bug));
    }
  }

  private createFromForm(): IBug {
    return {
      ...new Bug(),
      id: this.editForm.get(['id'])!.value,
      deviceId: this.editForm.get(['deviceId'])!.value,
      testerId: this.editForm.get(['testerId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBug>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
