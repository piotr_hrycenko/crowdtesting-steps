package me.phrycenko.crowdtesting.service;

import me.phrycenko.crowdtesting.domain.Tester;
import me.phrycenko.crowdtesting.repository.TesterRepository;
import me.phrycenko.crowdtesting.service.dto.TesterDTO;
import me.phrycenko.crowdtesting.service.mapper.TesterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Tester}.
 */
@Service
@Transactional
public class TesterService {

    private final Logger log = LoggerFactory.getLogger(TesterService.class);

    private final TesterRepository testerRepository;

    private final TesterMapper testerMapper;

    public TesterService(TesterRepository testerRepository, TesterMapper testerMapper) {
        this.testerRepository = testerRepository;
        this.testerMapper = testerMapper;
    }

    /**
     * Save a tester.
     *
     * @param testerDTO the entity to save.
     * @return the persisted entity.
     */
    public TesterDTO save(TesterDTO testerDTO) {
        log.debug("Request to save Tester : {}", testerDTO);
        Tester tester = testerMapper.toEntity(testerDTO);
        tester = testerRepository.save(tester);
        return testerMapper.toDto(tester);
    }

    /**
     * Get all the testers.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TesterDTO> findAll() {
        log.debug("Request to get all Testers");
        return testerRepository.findAll().stream()
            .map(testerMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one tester by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TesterDTO> findOne(Long id) {
        log.debug("Request to get Tester : {}", id);
        return testerRepository.findById(id)
            .map(testerMapper::toDto);
    }

    /**
     * Delete the tester by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Tester : {}", id);
        testerRepository.deleteById(id);
    }
}
