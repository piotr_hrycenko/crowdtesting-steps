package me.phrycenko.crowdtesting.web.rest;

import me.phrycenko.crowdtesting.CrowdtestingApp;
import me.phrycenko.crowdtesting.domain.Tester;
import me.phrycenko.crowdtesting.repository.TesterRepository;
import me.phrycenko.crowdtesting.service.TesterService;
import me.phrycenko.crowdtesting.service.dto.TesterDTO;
import me.phrycenko.crowdtesting.service.mapper.TesterMapper;
import me.phrycenko.crowdtesting.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static me.phrycenko.crowdtesting.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import me.phrycenko.crowdtesting.domain.enumeration.Country;
/**
 * Integration tests for the {@link TesterResource} REST controller.
 */
@SpringBootTest(classes = CrowdtestingApp.class)
public class TesterResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Country DEFAULT_COUNTRY = Country.GB;
    private static final Country UPDATED_COUNTRY = Country.BE;

    private static final LocalDate DEFAULT_LAST_LOGIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_LOGIN = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private TesterRepository testerRepository;

    @Autowired
    private TesterMapper testerMapper;

    @Autowired
    private TesterService testerService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTesterMockMvc;

    private Tester tester;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TesterResource testerResource = new TesterResource(testerService);
        this.restTesterMockMvc = MockMvcBuilders.standaloneSetup(testerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tester createEntity(EntityManager em) {
        Tester tester = new Tester()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .country(DEFAULT_COUNTRY)
            .lastLogin(DEFAULT_LAST_LOGIN);
        return tester;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tester createUpdatedEntity(EntityManager em) {
        Tester tester = new Tester()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .country(UPDATED_COUNTRY)
            .lastLogin(UPDATED_LAST_LOGIN);
        return tester;
    }

    @BeforeEach
    public void initTest() {
        tester = createEntity(em);
    }

    @Test
    @Transactional
    public void createTester() throws Exception {
        int databaseSizeBeforeCreate = testerRepository.findAll().size();

        // Create the Tester
        TesterDTO testerDTO = testerMapper.toDto(tester);
        restTesterMockMvc.perform(post("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isCreated());

        // Validate the Tester in the database
        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeCreate + 1);
        Tester testTester = testerList.get(testerList.size() - 1);
        assertThat(testTester.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testTester.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testTester.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testTester.getLastLogin()).isEqualTo(DEFAULT_LAST_LOGIN);
    }

    @Test
    @Transactional
    public void createTesterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = testerRepository.findAll().size();

        // Create the Tester with an existing ID
        tester.setId(1L);
        TesterDTO testerDTO = testerMapper.toDto(tester);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTesterMockMvc.perform(post("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tester in the database
        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = testerRepository.findAll().size();
        // set the field null
        tester.setFirstName(null);

        // Create the Tester, which fails.
        TesterDTO testerDTO = testerMapper.toDto(tester);

        restTesterMockMvc.perform(post("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isBadRequest());

        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = testerRepository.findAll().size();
        // set the field null
        tester.setLastName(null);

        // Create the Tester, which fails.
        TesterDTO testerDTO = testerMapper.toDto(tester);

        restTesterMockMvc.perform(post("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isBadRequest());

        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = testerRepository.findAll().size();
        // set the field null
        tester.setCountry(null);

        // Create the Tester, which fails.
        TesterDTO testerDTO = testerMapper.toDto(tester);

        restTesterMockMvc.perform(post("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isBadRequest());

        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTesters() throws Exception {
        // Initialize the database
        testerRepository.saveAndFlush(tester);

        // Get all the testerList
        restTesterMockMvc.perform(get("/api/testers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tester.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].lastLogin").value(hasItem(DEFAULT_LAST_LOGIN.toString())));
    }

    @Test
    @Transactional
    public void getTester() throws Exception {
        // Initialize the database
        testerRepository.saveAndFlush(tester);

        // Get the tester
        restTesterMockMvc.perform(get("/api/testers/{id}", tester.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tester.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.lastLogin").value(DEFAULT_LAST_LOGIN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTester() throws Exception {
        // Get the tester
        restTesterMockMvc.perform(get("/api/testers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTester() throws Exception {
        // Initialize the database
        testerRepository.saveAndFlush(tester);

        int databaseSizeBeforeUpdate = testerRepository.findAll().size();

        // Update the tester
        Tester updatedTester = testerRepository.findById(tester.getId()).get();
        // Disconnect from session so that the updates on updatedTester are not directly saved in db
        em.detach(updatedTester);
        updatedTester
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .country(UPDATED_COUNTRY)
            .lastLogin(UPDATED_LAST_LOGIN);
        TesterDTO testerDTO = testerMapper.toDto(updatedTester);

        restTesterMockMvc.perform(put("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isOk());

        // Validate the Tester in the database
        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeUpdate);
        Tester testTester = testerList.get(testerList.size() - 1);
        assertThat(testTester.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testTester.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testTester.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testTester.getLastLogin()).isEqualTo(UPDATED_LAST_LOGIN);
    }

    @Test
    @Transactional
    public void updateNonExistingTester() throws Exception {
        int databaseSizeBeforeUpdate = testerRepository.findAll().size();

        // Create the Tester
        TesterDTO testerDTO = testerMapper.toDto(tester);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTesterMockMvc.perform(put("/api/testers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tester in the database
        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTester() throws Exception {
        // Initialize the database
        testerRepository.saveAndFlush(tester);

        int databaseSizeBeforeDelete = testerRepository.findAll().size();

        // Delete the tester
        restTesterMockMvc.perform(delete("/api/testers/{id}", tester.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Tester> testerList = testerRepository.findAll();
        assertThat(testerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
