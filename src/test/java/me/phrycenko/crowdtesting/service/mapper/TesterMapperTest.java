package me.phrycenko.crowdtesting.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class TesterMapperTest {

    private TesterMapper testerMapper;

    @BeforeEach
    public void setUp() {
        testerMapper = new TesterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(testerMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(testerMapper.fromId(null)).isNull();
    }
}
