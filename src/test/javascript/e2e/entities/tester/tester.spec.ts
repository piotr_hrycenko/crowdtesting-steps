import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TesterComponentsPage, TesterDeleteDialog, TesterUpdatePage } from './tester.page-object';

const expect = chai.expect;

describe('Tester e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let testerComponentsPage: TesterComponentsPage;
  let testerUpdatePage: TesterUpdatePage;
  let testerDeleteDialog: TesterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Testers', async () => {
    await navBarPage.goToEntity('tester');
    testerComponentsPage = new TesterComponentsPage();
    await browser.wait(ec.visibilityOf(testerComponentsPage.title), 5000);
    expect(await testerComponentsPage.getTitle()).to.eq('crowdtestingApp.tester.home.title');
  });

  it('should load create Tester page', async () => {
    await testerComponentsPage.clickOnCreateButton();
    testerUpdatePage = new TesterUpdatePage();
    expect(await testerUpdatePage.getPageTitle()).to.eq('crowdtestingApp.tester.home.createOrEditLabel');
    await testerUpdatePage.cancel();
  });

  it('should create and save Testers', async () => {
    const nbButtonsBeforeCreate = await testerComponentsPage.countDeleteButtons();

    await testerComponentsPage.clickOnCreateButton();
    await promise.all([
      testerUpdatePage.setFirstNameInput('firstName'),
      testerUpdatePage.setLastNameInput('lastName'),
      testerUpdatePage.countrySelectLastOption(),
      testerUpdatePage.setLastLoginInput('2000-12-31')
    ]);
    expect(await testerUpdatePage.getFirstNameInput()).to.eq('firstName', 'Expected FirstName value to be equals to firstName');
    expect(await testerUpdatePage.getLastNameInput()).to.eq('lastName', 'Expected LastName value to be equals to lastName');
    expect(await testerUpdatePage.getLastLoginInput()).to.eq('2000-12-31', 'Expected lastLogin value to be equals to 2000-12-31');
    await testerUpdatePage.save();
    expect(await testerUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await testerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Tester', async () => {
    const nbButtonsBeforeDelete = await testerComponentsPage.countDeleteButtons();
    await testerComponentsPage.clickOnLastDeleteButton();

    testerDeleteDialog = new TesterDeleteDialog();
    expect(await testerDeleteDialog.getDialogTitle()).to.eq('crowdtestingApp.tester.delete.question');
    await testerDeleteDialog.clickOnConfirmButton();

    expect(await testerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
