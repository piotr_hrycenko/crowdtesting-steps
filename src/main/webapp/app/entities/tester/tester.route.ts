import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITester, Tester } from 'app/shared/model/tester.model';
import { TesterService } from './tester.service';
import { TesterComponent } from './tester.component';
import { TesterDetailComponent } from './tester-detail.component';
import { TesterUpdateComponent } from './tester-update.component';

@Injectable({ providedIn: 'root' })
export class TesterResolve implements Resolve<ITester> {
  constructor(private service: TesterService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITester> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tester: HttpResponse<Tester>) => {
          if (tester.body) {
            return of(tester.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Tester());
  }
}

export const testerRoute: Routes = [
  {
    path: '',
    component: TesterComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crowdtestingApp.tester.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TesterDetailComponent,
    resolve: {
      tester: TesterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crowdtestingApp.tester.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TesterUpdateComponent,
    resolve: {
      tester: TesterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crowdtestingApp.tester.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TesterUpdateComponent,
    resolve: {
      tester: TesterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'crowdtestingApp.tester.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
