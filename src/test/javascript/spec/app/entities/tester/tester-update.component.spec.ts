import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { CrowdtestingTestModule } from '../../../test.module';
import { TesterUpdateComponent } from 'app/entities/tester/tester-update.component';
import { TesterService } from 'app/entities/tester/tester.service';
import { Tester } from 'app/shared/model/tester.model';

describe('Component Tests', () => {
  describe('Tester Management Update Component', () => {
    let comp: TesterUpdateComponent;
    let fixture: ComponentFixture<TesterUpdateComponent>;
    let service: TesterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CrowdtestingTestModule],
        declarations: [TesterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TesterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TesterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TesterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Tester(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Tester();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
